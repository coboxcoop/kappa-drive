const Kappa = require('kappa-core')
const Indexer = require('kappa-sparse-indexer')
const Nanoresource = require('nanoresource/emitter')
const KV = require('kappa-view-kv')
const Corestore = require('corestore')
const Multifeed = require('@coboxcoop/multifeed')
const { MuxerTopic } = require('@coboxcoop/multifeed/networker')
const hyperdrive = require('hyperdrive')
const memdb = require('level-mem')
const duplexify = require('duplexify')
const through = require('through2')
const collect = require('collect-stream')
const unique = require('unique-stream')
const containsPath = require('contains-path')
const { codecs } = require('hypercore')
const crypto = require('hypercore-crypto')
const errors = require('hyperdrive/lib/errors')
const HypercoreDefaultStorage = require('hypercore-default-storage')
const sub = require('subleveldown')
const debug = require('debug')
const { Header, Node } = require('hypertrie/lib/messages')
const { Stat } = require('hyperdrive-schemas')
const datEncoding = require('dat-encoding')
const encoder = require('crypto-encoder')
const parallel = require('run-parallel')

const { normalize, logCpuMemory } = require('./util')
const { vectorClock, numberLinks } = require('./merge')
const { State } = require('./messages')
const { assign } = Object

const METADATA = 'metadata'
const CONTENT = 'content'

const BAD_FILE_DESCRIPTOR = 'kappa-drive: bad file descriptor'
const MISSING_FILE_DESCRIPTOR = 'kappa-drive: missing file descriptor'

const WRITEFLAGS = ['w']
const READFLAGS = ['r']
const RANDOMFLAGS = ['a', 'ax', 'as', 'wx', 'a+', 'ax+', 'r+', 'rs+', 'w+', 'wx+']

class KappaDrive extends Nanoresource {
  /**
   * @callback simpleErrorCallback
   * @param {object} err - an Error prototype
   *
   * @callback fileDesriptorCallback
   * @param {object} err - an Error prototype
   * @param {integer} fd - a file descriptor
   *
   * @callback readCallback
   * @param {object} err - an Error prototype
   * @param {integer} bytesRead - the number of bytes read
   * @param {buffer} buffer - a buffer containing the data
   *
   * @callback dataCallback
   * @param {object} err - an Error prototype
   * @param {string} data - a data string
   *
   * @callback arrayCallback
   * @param {object} err - an Error prototype
   * @param {Array} data - a data array
   *
   * @callback objectCallback
   * @param {object} err - an Error prototype
   * @param {object} data - a data object
   *
   * @callback driveCallback
   * @param {object} err - an Error prototype
   * @param {Hyperdrive} drive - a ready hyperdrive
   */

  /**
   * make a set of hyperdrives appear as one
   *
   * @param {string|Corestore} storage - a path to create a new corestore, or a corestore instance
   * @param {buffer|string} key - the key used to bootstrap multifeed's multiplexer and replication
   * @param {object} opts
   * @param {function} opts.logger - a custom logger function
   * @param {Multifeed} opts.feeds - a existing multifeed instance
   * @param {function} opts.deriveKeyPair - a libsodium key derivation function returns { publicKey, secretKey }
   * @param {object} opts.db - a leveldb instance for storing indexes
   * @param {function} opts.resolveFork - alternative function to handle file conflicts
   * @param {MuxerTopic} opts.muxer - a muxer topic instance handling multiple peer streams for a single multifeed
   * @param {Kappa} opts.core - a kappa-core instance
   */
  constructor (storage, key, opts) {
    if (!Buffer.isBuffer(key) && !opts) {
      opts = key
      key = null
    }

    if (!opts) opts = {}

    super()

    this.log = opts.logger || debug('kappa-drive')
    this._id = opts._id || hex(crypto.randomBytes(2))

    if (opts.feeds) this.key = opts.feeds.key
    else this.key = key || crypto.randomBytes(32)

    this.setMaxListeners(20)

    this.discoveryKey = crypto.discoveryKey(key)
    this.deriveKeyPair = opts.deriveKeyPair || noop
    this.destroyed = false

    this._db = opts.db || memdb()
    this._resolveFork = opts.resolveFork || numberLinks || vectorClock
    this._fds = new Map()
    this._drives = new Map()

    this._opts = opts

    this.corestore = defaultCorestore(storage, {
      ...opts,
      valueEncoding: 'binary',
      ifAvailable: true
    })

    this.muxer = opts.muxer || new MuxerTopic(this.corestore, this.key, {
      getFeed: this._onFeedKey.bind(this)
    })

    this._feeds = opts.feeds || new Multifeed(this.corestore, {
      ...opts,
      muxer: this.muxer,
      rootKey: this.key
    })

    this.core = opts.core || new Kappa()

    this._indexerStorage = sub(this._db, 'idx')
    this._indexer = opts.indexer || new Indexer({
      db: this._indexerStorage,
      name: this._id
    })

    this.vectorClock = {}

    this.metadataId = opts.metadataId || 1
    this.contentId = opts.contentId || 2

    // use keypair derivation for local feeds only
    this.localMetadataOpts = { ...opts, keyPair: this.deriveKeyPair(this.metadataId, this.key) }
    this.remoteMetadataOpts = assign({}, this.localMetadataOpts, { keyPair: undefined })
    this.localContentOpts = { ...opts, keyPair: this.deriveKeyPair(this.contentId, this.key) }
    this.remoteContentOpts = assign({}, this.localContentOpts, { keyPair: undefined })

    this._feeds.feeds().forEach(this._onFeed.bind(this))
    this._feeds.on('feed', this._onFeed.bind(this))
    this._feeds.on('download', this._onFeed.bind(this))
  }

  /*
   * open corestore, all hypercores and initialize indexes
   *
   * @param {simpleErrorCallback} callback
   */
  ready (callback) {
    return this.open((err) => {
      if (err) return callback(err)
      this._indexer.ready(() => {
        this.core.ready('drive', callback)
      })
    })
  }

  /*
   * replicate across the hypercore protocol using multifeed's multiplexer
   *
   * @param {boolean} isInitiator - used by hypercore-protocol, indicating if you are a client or server
   * @param {object} opts
   * @param {boolean} opts.live - keep the protocol replication stream open for live updates
   */
  replicate (isInitiator, opts = {}) {
    return this._feeds.replicate(isInitiator, {
      live: true,
      maxFeeds: 1024,
      ...opts
    })
  }

  /**
   * implements fs.open to open a file with access flag (read/write/random)
   * see: https://nodejs.org/api/fs.html#fs_fs_open_path_flags_mode_callback
   *
   * function name clash, fs.open vs. nanoresource.open,
   * nanoresource ensures function can only be called once
   * until it returns. super called to pass callback down,
   * otherwise fs operations are handled normally
   *
   * @param {string} filename - path to a file in your kappa-drive
   * @param {string|integer} flags - read, write or random-access flag
   * @param {fileDescriptorCallback} callback
   */
  open (filename, flags, callback) {
    if (!filename || typeof filename === 'function') return super.open(filename)

    const handler = this._getFlagHandler(flags)
    if (!handler) return callback(new Error(`kappa-drive: missing flag handler for flag ${flags}`))
    handler(filename, flags, callback)
  }

  /**
   * implements fs.close to close the file descriptor
   * see: https://nodejs.org/api/fs.html#fs_fs_close_fd_callback
   *
   * function name clash, fs.close vs. nanoresource.close,
   * fs operations are handled normally when fd is passed
   *
   * @param {integer} fd - a file descriptor
   * @param {simpleErrorCallback} callback
   */
  close (fd, callback) {
    if (typeof fd === 'number') return this._closeFile(fd, callback)
    super.close(false, fd)
  }

  /**
   * close an open file descriptor and it's hyperdrive
   *
   * @param {integer} fd - a file descriptor
   * @param {simpleErrorCallback} callback
   */
  _closeFile (fd, callback) {
    const descriptor = this._fds.get(fd)
    if (!descriptor) return callback(new errors.BadFileDescriptor(MISSING_FILE_DESCRIPTOR))
    const { filename, writeMode } = descriptor
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))

    if (writeMode) {
      this._getLinks(filename, (err, links) => {
        if (err) return callback(err)
        this.drive.close(fd, (err) => {
          if (err) return callback(err)
          this._fds.delete(fd)
          this._finishWrite(filename, links, callback)
        })
      })
    } else {
      const { driveKey } = this._fds.get(fd)
      const drive = this._drives.get(driveKey)
      drive.close(fd, (err) => {
        if (err) return callback(err)
        this._fds.delete(fd)
        return callback()
      })
    }
  }

  /**
   * copy a file from a source to a destination path
   *
   * @param {string} from - a source file
   * @param {string} to - a destination file
   * @param {simpleErrorCallback} cb
   */
  copy (from, to, cb) {
    this.stat(from, (err, stat) => {
      if (err) return cb(err)
      this.create(to, stat, cb)
    })
  }

  // -------------- READ ACTIONS ----------------

  /**
   * implements fs.read to read a file given an open file descriptor
   * see: https://nodejs.org/api/fs.html#fs_fs_read_fd_buffer_offset_length_position_callback
   *
   * @param {integer} fd - a file descriptor
   * @param {buffer} buf - buffer that the data will be written to
   * @param {integer} offset - the position in buffer to write the data to
   * @param {integer} length - the number of bytes to read
   * @param {integer} position - specifies where to begin reading from in the file
   * @param {simpleErrorCallback} callback
   */
  read (fd, buf, offset, len, position, callback) {
    const descriptor = this._fds.get(fd)
    if (!descriptor) return callback(new errors.BadFileDescriptor(MISSING_FILE_DESCRIPTOR))

    const { filename, writeMode, driveKey } = descriptor
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))

    if (writeMode) return this.drive.read(fd, buf, offset, len, position, callback)
    const drive = this._drives.get(driveKey)
    return drive.read(fd, buf, offset, len, position, callback)
  }

  /**
   * implements fs.readFile to read a file using a path string
   * see: https://nodejs.org/api/fs.html#fs_fs_readfile_path_options_callback
   *
   * @param {string} filename - path to the file
   * @param {object} opts - additional options for hyperdrive.readFile
   * @param {dataCallback} callback
   */
  readFile (filename, opts, callback) {
    if (typeof opts === 'function') return this.readFile(filename, null, opts)
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.readFile(filename, opts, callback)
    })
  }

  /**
   * implements fs.createReadStream to stream file contents using a path string
   * see: https://nodejs.org/api/fs.html#fs_fs_createreadstream_path_options
   *
   * @param {string} filename - path to the file
   * @returns {Readable} stream - a readable stream of file data
   */
  createReadStream (filename) {
    var proxy = duplexify()
    this._whoHasFile(filename, (err, drive) => {
      if (err) return proxy.emit('err', err)
      proxy.setReadable(drive.createReadStream(filename))
    })
    return proxy
  }

  /**
   * implements fs.exists to check the existence of a file using a path string
   * see: https://nodejs.org/api/fs.html#fs_fs_exists_path_callback
   *
   * @param {string} filename - path to the file
   * @param {object} opts - additional options for hyperdrive.exists
   * @param {simpleErrorCallback}
   */
  exists (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.exists(filename, opts, callback)
    })
  }

  /**
   * implements fs.readdir to read the contents of a directory
   * see: https://nodejs.org/api/fs.html#fs_fs_readdir_path_options_callback
   *
   * @param {string} name - path to the directory
   * @param {object} opts - additional options for hyperdrive.exists
   * @param {arrayCallback} callback
   */
  readdir (name, opts, callback) {
    if (typeof opts === 'function') return this.readdir(name, null, opts)

    name = normalize(name)

    var self = this

    this.core.ready('drive', () => {
      var stream = this.core.view.drive.createReadStream()
      var t = stream.pipe(through.obj(function (chunk, enc, next) {
        if (!containsPath(chunk.key, `./${name}`)) return next()

        self.exists(chunk.key, opts, (exists) => {
          if (exists) {
            const filePath = normalize(chunk.key.substring(name.length))
            const filename = filePath.split('/')[0]
            if (filename !== '') this.push({ filename })
          }
          next()
        })
      }))

      collect(t.pipe(unique()), (err, data) => {
        if (err) return callback(err)
        var files = data.map(d => d.filename)
        callback(null, files)
      })
    })
  }

  /**
   * implements fs.stat to check the existence of a file before opening
   * see: https://nodejs.org/api/fs.html#fs_fs_stat_path_options_callback
   *
   * @param {string} name - path to the file
   * @param {object} opts - additional options for hyperdrive.exists
   * @param {objectCallback} callback
   */
  stat (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.stat(filename, opts, callback)
    })
  }

  /**
   * implements fs.lstat to read the symlink referred to by the path
   * see: https://nodejs.org/api/fs.html#fs_fs_lstat_path_options_callback
   *
   * @param {string} name - path to the file
   * @param {object} opts - additional options for hyperdrive.exists
   * @param {objectCallback} callback
   */
  lstat (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.lstat(filename, opts, callback)
    })
  }

  // --------------- WRITE ACTIONS -----------------

  /**
   * implements fs.write to write to a file given an open file descriptor
   * see: https://nodejs.org/api/fs.html#fs_fs_write_fd_buffer_offset_length_position_callback
   *
   * @param {integer} fd - a file descriptor
   * @param {buffer} buf - buffer that the data will be written to
   * @param {integer} offset - the position in buffer to write the data to
   * @param {integer} length - the number of bytes to read
   * @param {integer} position - specifies where to begin reading from in the file
   * @param {simpleErrorCallback} callback
   */
  write (fd, buf, offset, length, position, callback) {
    if (typeof position === 'function' && !callback) return this.write(fd, buf, offset, length, null, position)

    const descriptor = this._fds.get(fd)
    if (!descriptor) return callback(new errors.BadFileDescriptor(MISSING_FILE_DESCRIPTOR))
    const { filename } = descriptor
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))
    this.drive.write(fd, buf, offset, length, position, callback)
  }

  /**
   * implements fs.writeFile to write some data to a specified file
   * see: https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback
   *
   * @param {string} filename - path to the file
   * @param {string|buffer} data - the file data to be written
   * @param {simpleErrorCallback} callback
   */
  writeFile (filename, data, callback) {
    this._getLinks(filename, (err, links) => {
      if (err) return callback(err)
      this.drive.writeFile(filename, data, (err) => {
        if (err) return callback(err)
        this._finishWrite(filename, links, callback)
      })
    })
  }

  /**
   * implements fs.createWriteStream to write file data to the drive as a stream
   * see: https://nodejs.org/api/fs.html#fs_fs_createwritestream_path_options
   *
   * @param {string} filename - path to the file
   * @returns {Writable} stream - a writeable stream of file data
   */
  createWriteStream (filename) {
    var proxy = duplexify()
    this._getLinks(filename, (err, links) => {
      if (err) proxy.emit('error', err)
      var writer = this.drive.createWriteStream(normalize(filename))
      proxy.setWritable(writer)

      var prefinish = () => {
        proxy.cork()
        this._finishWrite(filename, links, (err) => {
          if (err) return proxy.destroy()
          proxy.uncork()
        })
      }

      proxy.on('close', done)
      proxy.on('finish', done)
      proxy.on('prefinish', prefinish)

      function done () {
        proxy.removeListener('close', done)
        proxy.removeListener('finish', done)
        proxy.removeListener('prefinish', prefinish)
      }
    })

    return proxy
  }

  /**
   * implements fs.symlink to create a symbolic link to a file
   * see: https://nodejs.org/api/fs.html#fs_fs_symlink_target_path_type_callback
   *
   * @param {string} target - path to the link file
   * @param {string} dest - path to the existing file
   * @param {simpleErrorCallback} callback
   */
  symlink (target, dest, callback) {
    this._getLinks(target, (err, links) => {
      if (err) return callback(err)
      this.drive.symlink(target, dest, (err) => {
        if (err) return callback(err)
        this._finishWrite(target, links, callback)
      })
    })
  }

  /**
   * implements fs.unlink to remove a symbolic link to a file
   * see: https://nodejs.org/api/fs.html#fs_fs_unlink_path_callback
   *
   * @param {string} target - path to the link file
   * @param {simpleErrorCallback} callback
   */
  unlink (target, callback) {
    this._getLinks(target, (err, links) => {
      if (err) return callback(err)
      if (!links.length) return callback(new errors.FileNotFound(target))
      // Do a write action, otherwise unlink will throw an error
      this.drive.writeFile(target, '', (err) => {
        if (err) return callback(err)
        this.drive.unlink(target, callback)
      })
    })
  }

  /**
   * implements fs.truncate to truncate a file
   * see: https://nodejs.org/api/fs.html#fs_fs_truncate_path_len_callback
   *
   * check who has the latest version, if I do, simply truncate the file,
   * if someone else, we want write a new truncated version of that file
   *
   * @param {string} filename - path to the file
   * @param {integer} size - number of bytes to truncate
   * @param {simpleErrorCallback} callback
   */
  truncate (filename, size, callback) {
    const self = this

    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      if (drive.key === this.drive.key) return performTruncate()
      drive.open(filename, 'r', (err, fd) => {
        if (err) return callback(err)
        const data = Buffer.alloc(size)
        drive.read(fd, data, 0, size, null, (err) => {
          if (err) return callback(err)
          drive.close(fd, (err) => {
            if (err) return callback(err)
            this._getLinks(filename, (err, links) => {
              if (err) return callback(err)
              this.drive.writeFile(filename, data, (err) => {
                if (err) return callback(err)
                this._finishWrite(filename, links, callback)
              })
            })
          })
        })
      })

      function performTruncate () {
        self._getLinks(filename, (err, links) => {
          if (err) return callback(err)
          self.drive.truncate(filename, size, (err) => {
            if (err) return callback(err)
            self._finishWrite(filename, links, callback)
          })
        })
      }
    })
  }


  /**
   * TODO: implements fs.ftruncate to truncate a file descriptor
   * see: https://nodejs.org/api/fs.html#fs_fs_ftruncate_fd_len_callback
   *
   * @param {object} fd - path to the file
   * @param {integer} size - number of bytes to truncate
   * @param {simpleErrorCallback} callback
   */
  ftruncate (fd, size, cb) {
    // We assume the fd refers to our drive..?
    var desc = this._fds.get(fd)
    if (!desc) return process.nextTick(cb, new errors.BadFileDescriptor(`Bad file descriptor: ${fd}`))
    desc.ftruncate(fd, size, cb)
  }

  /**
   * implements fs.rename to rename a file
   * see: https://nodejs.org/api/fs.html#fs_fs_rename_oldpath_newpath_callback
   *
   * reads from the source file and writes a new version to the destination
   * then unlinks the source file. source file data is preseved.
   *
   * @param {string} source - a source file
   * @param {string} destination - a destination file
   * @param {simpleErrorCallback} callback
   */
  rename (source, destination, callback) {
    this.readFile(source, (err, data) => {
      if (err) return callback(err)
      this.writeFile(destination, data, (err) => {
        if (err) return callback(err)
        this.unlink(source, (err) => {
          if (err) return callback(err)
          callback()
        })
      })
    })
  }

  /**
   * implements fs.mkdir to create a directory
   * see: https://nodejs.org/api/fs.html#fs_fs_mkdir_path_options_callback
   * NOTE: hyperdrive's mkdir doesnt take mode, fs.mkdir does
   *
   * @param {string} name - a directory path
   * @param {string|integer} mode - N/A
   * @param {simpleErrorCallback} callback
   */
  mkdir (name, mode, callback) {
    if (typeof mode === 'function') return this.mkdir(name, null, mode)
    // TODO: maybe this depends on previous state (should throw err if already exists)
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive.mkdir(name, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  /**
   * implements fs.rmdir to remove a directory
   * see: https://nodejs.org/api/fs.html#fs_fs_rmdir_path_options_callback
   * NOTE: only applies to directories we've created
   *
   * @param {string} name - a directory path
   * @param {simpleErrorCallback} callback
   */
  rmdir (name, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      if (!links.length) return callback(new errors.FileNotFound(name))
      // See if we have it, otherwise rmdir throws an error
      this.drive.stat(name, (err, statObj) => {
        if (err) return this._finishWrite(name, links, callback)
        this.drive.rmdir(name, callback)
      })
    })
  }

  /**
   * creates a file stat in local drive
   * https://github.com/hypercore-protocol/hyperdrive/blob/master/index.js#L549
   *
   * @param {string} name - a directory path
   * @param {object} opts - an options object
   * @param {simpleErrorCallback} callback
   */
  create (name, opts, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive.create(name, opts, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  /**
   * destroy all local hyperdrives in this kappa-drive
   * this will permanently delete your drive contents,
   * removing both metadata and content feeds from corestore
   * https://github.com/hypercore-protocol/hyperdrive/blob/master/index.js#L873
   *
   * @param {simpleErrorCallback} callback
   */
  destroyStorage (callback) {
    let tasks = []
    const persistenceFeed = this._feeds._handlers.feed
    tasks.push(initDestroy(persistenceFeed))
    tasks.push(initDestroy(this.drive))
    Object.values(this._drives).forEach((drive) => tasks.push(initDestroy(drive)))

    parallel(tasks, () => {
      this._db.clear((err) => {
        if (err) return callback(err)
        this.destroyed = true
        this._close(callback)
      })
    })

    function initDestroy (destroyable) {
      return (done) => destroyable.destroyStorage(done)
    }
  }

  // -------------------- PRIVATE FUNCTIONS -----------------------

  /**
   * update a file reference in the metadata feed
   * this is used when changing permissions using chmod and chown
   * https://github.com/hypercore-protocol/hyperdrive/blob/master/index.js#L296
   *
   * @param {string} name - a file or directory path
   * @param {object} opts - an options object
   * @param {simpleErrorCallback} callback
   */
  _update (name, opts, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive._update(name, opts, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  /**
   * query our backlinks materialized view for all references to a file
   * validate each message is properly encoded, then apply a conflict
   * resolution mechanism, default uses the last unix timestamp,
   * fetch the winning drive reference, fetch the correct feeds,
   * and reconstruct / load the drive from the cache. this is
   * performed on reads only
   *
   * @param {string} filename - a file path
   * @paran {simpleErrorCallback} callback
   */
  _whoHasFile (filename, callback) {
    filename = normalize(filename)

    this.core.ready('drive', () => {
      this.core.view.drive.get(filename, (err, msgs = []) => {
        if (err && !err.notFound) return callback(err)
        const values = msgs.map((msg) => tryDecode(msg.value))
        if (!values || !values.length) return callback(null, this.drive)
        const winner = this._resolveFork(values, this.vectorClock)

        this._areValidKeys(winner.metadata.id, winner.content.id, (err) => {
          if (err) return callback(err)

          this._getDrive(winner.metadata.id, winner.content.id, {
            ...this.remoteMetadatOpts,
            contentOpts: this.remoteContentOpts
          }, callback)
        })
      })
    })
  }

  /*
   * check if corestore has metadata and content feeds by their discovery keys
   * @param {string} metadataKey - key of a metadata key
   * @param {string} contentKey - key of a metadata key
   * @param {function(err)} callback - a callback to run where an error is thrown if either key is invalid
   */
  _areValidKeys (metadataKey, contentKey, callback) {
    var metadataDiscKey = crypto.discoveryKey(datEncoding.decode(metadataKey))
    var contentDiscKey = crypto.discoveryKey(datEncoding.decode(contentKey))

    this.corestore.inner._checkIfExists(metadataDiscKey, (err, metadataExists) => {
      if (err || !metadataExists) return callback(err || new Error('kappa-drive: invalid key for metadata'))
      this.corestore.inner._checkIfExists(contentDiscKey, (err, contentExists) => {
        if (err || !contentExists) return callback(err || new Error('kappa-drive: invalid key for content'))
        return callback()
      })
    })
  }

  /**
   * query our backlinks materialized view for all references to a file
   * then compile a list of links of the format feedkey@seq
   *
   * @param {string} filename - path to a file
   * @param {arrayCallback} callback
   */
  _getLinks (filename, callback) {
    filename = normalize(filename)
    this.core.ready('drive', () => {
      this.core.view.drive.get(filename, (err, msgs) => {
        if (err && !err.notFound) return callback(err)
        const links = msgs ? msgs.map((msg) => [msg.key, msg.seq].join('@')) : []
        return callback(null, links)
      })
    })
  }

  /**
   * updating drive metadata feed with the latest state of
   * this file (from our perspective), this is used by all
   * drive peers to determine a shared state or causal
   *
   * @param {string} filename - the name of the changed file
   * @param {Array} links - an array of file backlinks
   * @param {simpleErrorCallback} callback
   */
  _finishWrite (filename, links, callback) {
    this.vectorClock[this.metadata.key.toString('hex')] += 1

    const state = {
      filename: normalize(filename),
      version: this.drive.version,
      links,
      metadata: {
        id: this.metadata.key.toString('hex'),
        seq: this.metadata.length
      },
      content: {
        id: this.content.key.toString('hex'),
        seq: this.content.length
      },
      timestamp: Date.now(),
      vectorClock: this.vectorClock
    }

    this.log(logCpuMemory(this._id))

    var metadata = { peerfs: State.encode(state) }

    this.drive._update(filename, { metadata }, callback)
  }

  /**
   * fetch a cached hyperdrive, or create a new one,
   * given a metadata and content feed provided by the index
   *
   * @param {string|buffer} metadataKey - a 32 byte metadata key
   * @param {string|buffer} contentKey - a 32 byte content key
   * @param {object} opts - an options object
   * @param {driveCallback} callback
   */
  _getDrive (metadataKey, contentKey, opts = {}, callback) {
    const self = this
    let drive = this._drives.get(metadataKey)
    if (drive) return drive.ready(done)

    drive = hyperdrive(this.corestore, metadataKey, opts)

    drive.ready((err) => {
      if (err) return callback(err)
      this._drives.set(drive.key, drive)
      return done(null, drive)
    })

    function done (err, drive) {
      if (err) return callback(err)

      drive.getContent((err, content) => {
        if (err) return callback(err)
        if (!self._feeds.feed(content)) self._feeds._addFeed(content, CONTENT, true)
        callback(err, drive)
      })
    }
  }

  /**
   * initialize a kappa-drive only once (see Nanoresource)
   * @callback {simpleErrorCallback}
   */
  _open (callback) {
    if (this.destroyed) return callback(new Error('kappa-drive: feeds destroyed'))
    this._initializeFeeds((err) => {
      if (err) return callback(err)
      this._initializeDrive(callback)
    })
  }

  /**
   * close leveldb index and kappa-core
   * @param {simpleErrorCallback} callback
   */
  _closeIndexes (callback) {
    this._db.close((err) => {
      if (err) return callback(err)
      this.core.close(callback)
    })
  }

  /**
   * close all hyperdrives in parallel
   * @param {simpleErrorCallback} callback
   */
  _closeDrives (callback) {
    let tasks = []
    tasks.push(closeDrive(this.drive))
    Object.values(this._drives).forEach((drive) => tasks.push(closeDrive(drive)))

    parallel(tasks, callback)

    function closeDrive (drive) {
      return (done) => drive.close(done)
    }
  }

  /**
   * close a kappa-drive only once (see Nanoresource)
   * @param {simpleErrorCallback} callback
   */
  _close (callback = noop) {
    this._closeIndexes((err) => {
      if (err) return callback(err)
      this._feeds.close(callback)
    })
  }

  /**
   * setup multifeed, then construct local
   * content and metadata hypercore feeds
   * cache in multifeed and cache locally
   *
   * @param {simpleErrorCallback} callback
   */
  _initializeFeeds (callback) {
    this._feeds.ready((err) => {
      if (err) return callback(err)

      this._feeds.writer(METADATA, this.localMetadataOpts, (err, metadata) => {
        if (err) return callback(err)
        this.metadata = metadata

        this._feeds.writer(CONTENT, this.localContentOpts, (err, content) => {
          if (err) return callback(err)
          this.content = content

          return callback()
        })
      })
    })
  }

  /**
   * construct then cache a local hyperdrive,
   * initialize the backlinks materialized view
   * and add to our kappa-core instance
   *
   * @param {simpleErrorCallback} callback
   */
  _initializeDrive (callback) {
    this._getDrive(this.metadata.key, this.content.key, {
      ...this.localMetadataOpts,
      contentOpts: this.localContentOpts
    }, (err, drive) => {
      if (err) return callback(err)

      this.drive = drive

      this.log({
        id: this._id,
        metadata: this.metadata.key,
        content: this.content.key,
        drive: this.drive.key
      })

      const view = KV(sub(this._db, 'view'), this._createMap(), {
        getFeed: this._getFeedByKey.bind(this)
      })

      this.core.use('drive', this._indexer.source(), view)

      this.core.view.drive.onUpdate((key, entry) => {
        this.emit('update', key, entry)
        this.drive.key.toString('hex') === entry.key
          ? this.emit('local-update', key, entry)
          : this.emit('remote-update', key, entry)
      })

      return callback()
    })
  }

  // ---------------- Read / Write / Random Access Flag Handlers -----------------

  /**
   * fetch the correct handler given
   * different access flags, read/write/random
   *
   * @param {string|number} flag - access mode flag (r, w, w+, etc)
   */
  _getFlagHandler (flag) {
    if (typeof flag === 'number') {
      const mode = flag & 3
      if (mode === 0) return this._handleRead.bind(this)
      return this._handleRandomAccess.bind(this)
    }

    const api = {
      [READFLAGS]: this._handleRead.bind(this),
      [WRITEFLAGS]: this._handleWrite.bind(this),
      [RANDOMFLAGS]: this._handleRandomAccess.bind(this)
    }

    const action = Object.keys(api)
      .map((flags) => flags.split(','))
      .find((flags) => flags.includes(flag.toString()))

    const handler = api[action]
    if (!handler) throw new Error(`kappa-drive: missing handler for ${flag}`)

    return handler
  }

  /**
   * if opening a file for write, we must
   * cache a writable file descriptor for this file
   * so we can handle the write operation
   *
   * @param {string} filename - path to file
   * @param {string|number} flags - write access flags
   * @param {fileDescriptorCallback} callback
   */
  _handleWrite (filename, flags, callback) {
    this.drive.open(filename, flags, (err, fd) => {
      if (err) return callback(err)
      this._fds.set(fd, { filename, writeMode: true })
      return callback(null, fd)
    })
  }

  /**
   * when opening a file for read, we must
   * cache the drive key, so we know which drive
   * to close after the read operation is performed
   *
   * @param {string} filename - path to file
   * @param {string|number} flags - read access flags
   * @param {fileDescriptorCallback} callback
   */
  _handleRead (filename, flags, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.open(filename, flags, (err, fd) => {
        if (err) return callback(err)
        this._fds.set(fd, { filename, writeMode: false, driveKey: drive.key })
        return callback(null, fd)
      })
    })
  }

  /**
   * when opening a file for random access
   * select the drive with the latest file version
   * read that file, write a copy to our own
   * drive, then handle write operation
   *
   * @param {string} filename - path to file
   * @param {string|number} flags - read access flags
   * @param {simpleErrorCallback} callback
   */
  _handleRandomAccess (filename, flags, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err || drive.metadata.key === this.drive.metadata.key) return this._handleWrite(filename, flags, callback)
      drive.readFile(filename, (err, data) => {
        if (err) return this._handleWrite(filename, flags, callback)
        this.writeFile(filename, data, (err) => {
          if (err) return callback(err)
          this._handleWrite(filename, flags, callback)
        })
      })
    })
  }

  // ------------------------ Others ---------------------------

  /**
   * when a new feed is added to multifeed
   * either locally or remote, add only
   * the metadata feed to our kappa indexer
   */
  _onFeed (feed) {
    this.log({ msg: 'new feed', id: this._id })
    if (this._indexer.feed(feed.key)) return
    scopeFeeds(feed, (err) => {
      this.log({ msg: 'adding to index', id: this._id, key: hex(feed.key) })
      if (!err) this._indexer.add(feed, { scan: true })
    })
  }

  /**
   * when replicating, fetch the correct feed
   * from our corestore instance
   * @param {string|buffer} key - a hypercore feed key
   * @param {objectCallback} callback - return a ready hypercore
   */
  _onFeedKey (key, cb) {
    var feed = this.corestore.get({ key, ...this._opts })
    return feed.ready((err) => cb(err, feed))
  }

  /**
   * when indexing, fetch the correct feed
   * @param {string|buffer} key - a hypercore feed key
   */
  _getFeedByKey (key) {
    return this._feeds.feed(key)
  }

  /**
   * kappa-view-kv index mapper, reads file state object
   * from metadata feed and compiles causal state
   */
  _createMap () {
    const self = this
    const ownKey = this.metadata.key.toString('hex')
    this.vectorClock[ownKey] = this.vectorClock[ownKey] || 0

    return function (msg, next) {
      const ops = []

      const state = tryDecode(msg.value)
      if (!state) return next()

      if (state.metadata.id !== ownKey) {
        self.vectorClock[ownKey] += 1
        self.vectorClock = combineVectors(self.vectorClock, state.vectorClock)
      }

      ops.push({
        key: state.filename,
        id: [msg.key, msg.seq].join('@'),
        links: (state.links || []),
        metadata: {
          id: state.metadata.id.toString('hex'),
          seq: state.metadata.seq
        },
        content: {
          id: state.content.id.toString('hex'),
          seq: state.content.seq
        },
        vectorClock: state.vectorClock
      })

      next(null, ops)
    }
  }
}

function combineVectors (a, b) {
  const result = {}
  Object.keys(a).forEach(key => {
    result[key] = Math.max(a[key], b[key] || 0)
  })
  Object.keys(b).forEach(key => {
    result[key] = result[key] || b[key]
  })
  return result
}

function tryDecode (value) {
  let stat = decode(Stat, value)
  if (!stat) {
    const node = decode(Node, value)
    if (node && node.valueBuffer) {
      stat = decode(Stat, node.valueBuffer)
    }
  }

  let state = null
  if (stat && stat.metadata && stat.metadata.peerfs) {
    state = decode(State, stat.metadata.peerfs)
  }

  return state
}

function decode (encoding, value) {
  try {
    return encoding.decode(value)
  } catch (err) {
    return null
  }
}

function scopeFeeds (feed, next) {
  feed.get(0, (err, msg) => {
    if (err) return next()

    try {
      const header = Header.decode(msg)
      if (header.type !== 'hypertrie') return next(new Error('invalid feed'))
      next()
    } catch (err) {
      return next(err)
    }
  })
}

function noop () {}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

function isCorestore (storage) {
  return storage.default && storage.get && storage.replicate && storage.close
}

function defaultCorestore (storage, opts) {
  if (isCorestore(storage)) return storage
  var factory
  if (typeof storage === 'function') {
    factory = (path) => storage(path)
  } else if (typeof storage === 'string') {
    factory = (path) => HypercoreDefaultStorage(storage + '/' + path)
  }
  return new Corestore(factory, opts)
}

module.exports = (storage, key, opts) => new KappaDrive(storage, key, opts)
module.exports.KappaDrive = KappaDrive
module.exports.tryDecode = tryDecode
module.exports.scopeFeeds = scopeFeeds
