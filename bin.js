#!/usr/bin/env node
const minimist = require('minimist')
const chalk = require('chalk')
const RAM = require('random-access-memory')
const path = require('path')
const level = require('level')
const memdb = require('level-mem')
const mkdirp = require('mkdirp')
const mount = require('kappa-drive-mount')
const sodium = require('sodium-universal')
const encoder = require('crypto-encoder')
const crypto = require('hypercore-crypto')
const CoreNetworker = require('@corestore/networker')
const FeedNetworker = require('@coboxcoop/multifeed/networker')
const KappaDrive = require('./')

Application(minimist(process.argv.slice(2)))

function Application (opts) {
  let valueEncoding, encryptionKey

  if (opts.help || opts.h) {
    console.log(usage())
    process.exit(0)
  }

  if (opts.new || opts.n) {
    opts.key = hex(crypto.randomBytes(32))
  }

  if (opts.encrypt || opts.e) {
    if (opts.passphrase) opts.encryptionKey = opts.passphrase
    else opts.encryptionKey = hex(crypto.randomBytes(32))
  }

  if (opts.encryptionKey) {
    encryptionKey = Buffer.from(opts.encryptionKey, 'hex')
    valueEncoding = encoder(encryptionKey, {
      nonce: encryptionKey.slice(0, encoder.NONCEBYTES),
      valueEncoding: 'binary'
    })
    delete opts.encryptionKey
  }

  if (!opts.mount) {
    opts.mount = opts.mnt || opts.m || './mnt'
  }

  if (!opts.key && !opts.k) {
    console.log(chalk.red('error:'), 'you must provide an argument:', chalk.blue('--key'), 'or', chalk.blue('--new'))
    console.log()
    console.log(usage())
    process.exit(1)
  }

  console.log('\n ', chalk.green.underline('Kappa-Drive'), '(also known as peerfs)')

  var storage = defaultStorage(opts.db)
  var views = defaultViews(storage)

  var key = Buffer.from(opts.key || opts.k, 'hex')
  var drive = KappaDrive(storage, key, {
    db: views,
    valueEncoding: valueEncoding || 'binary'
  })

  drive.ready((err) => {
    if (err) return console.error(err)

    console.log(`
  Drive Key: ${chalk.blue(hex(drive.key))} 
  Swarm Address: ${chalk.blue(hex(drive.discoveryKey))}${encryptionKey ? `\n  Encryption Key: ${chalk.blue(hex(encryptionKey))}` : ''}`)

    swarm(drive)

    mount(drive, opts.mount, (err, unmount) => {
      if (err) return console.error(err)

      console.log(`
  Storage: ${isFunction(storage) ? chalk.green('in memory') : `${chalk.green(`file://${path.resolve(storage)}`)}`}
  Mount: ${chalk.green(`file://${path.resolve(opts.mount)}`)}`)

      process.once('SIGINT', () => {
        console.log(chalk.red('\n  Dropping peer connections and exiting the swarm...'))
        console.log(chalk.red('  Terminating FUSE mount...'))
        unmount((err) => {
          if (err) return console.error(err)
          console.log(chalk.red('  Closing the drive...'))
          drive.close()
          console.log(`\n  ${chalk.green('Success!')}\n\n  Thanks for using Kappa-Drive.`)
          process.exit(0)
        })
      })
    })
  })

  function usage () {
    return `Usage

  Create a new kappa-drive
    kappa-drive --new

  Join a kappa-drive by its key
    kappa-drive --key {key}

  Options:
    --new                 Start a new kappa-drive
    --key                 Specify a specific key / address / topic. default: ${chalk.green('bee80ff3a4ee5e727dc44197cb9d25bf8f19d50b0f3ad2984cfe5b7d14e75de7$')}
    --encrypt, -e         Generate or use a given encryption key. default: ${chalk.green('none')}
    --mnt                 Choose a mount directory for your kappa-drive. default: ${chalk.green('"./mnt"')}
    --db                  Specify a storage path for kappa-drive's hyperdrives. default: ${chalk.green('RAM')}
    --help                Print this help message

Learn more at ${chalk.blue('https://gitlab.com/coboxcoop/kappa-drive')}`
  }
}

function swarm (drive, opts = {}) {
  const networker = new CoreNetworker(drive.corestore)
  const multinet = new FeedNetworker(networker)

  networker.listen()

  networker.on('peer-add', (peer) => {
    printPeer(peer, 'Connection')
  })

  networker.on('peer-remove', (peer) => {
    printPeer(peer, 'Disconnection')
  })

  return multinet.swarm(drive._feeds, {
    live: true,
    ...opts
  })
}

function printPeer (peer, message) {
  if (!peer) return
  console.log(`
  ${chalk.green(message)}
  Peer ID: ${chalk.blue(hex(peer.remotePublicKey))}`)
}

function defaultStorage (storage) {
  if (storage) return path.resolve(storage)
  return RAM
}

function defaultViews (storage) {
  if (isFunction(storage)) return memdb()
  var views = path.join(path.resolve(storage), 'views')
  mkdirp.sync(views)
  return level(views)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

function genericHash (input) {
  var hash = Buffer.alloc(sodium.crypto_generichash_BYTES_MIN)
  sodium.crypto_generichash(hash, Buffer.from(input))
  return hash
}

function isFunction (a) {
  return typeof a === 'function'
}
