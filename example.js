const KappaDrive = require('.')
var aliceDrive = KappaDrive('./alice-drive')
var bobDrive = KappaDrive('./bob-drive')

aliceDrive.ready(() => {
  aliceDrive.writeFile('/hello.txt', 'world', (err) => {
    if (err) throw err
    bobDrive.ready(() => {
      replicate(bobOverwrite)
    })
  })
})

function bobOverwrite () {
  bobDrive.writeFile('/hello.txt', 'mundo', (err) => {
    if (err) throw err
    check()
  })
}

function replicate (cb) {
  var aliceStream = aliceDrive.replicate(true, { live: false })
  aliceStream.pipe(bobDrive.replicate(false, { live: false })).pipe(aliceStream)
  aliceStream.on('end', cb)
}

function check () {
  replicate(() => {
    aliceDrive.readFile('/hello.txt', 'utf-8', (err, data) => {
      if (err) throw err
      console.log(data) // mundo
    })
  })
}
