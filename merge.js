function dumbMerge (values) {
  return values[0]
}

function vectorClock (values, vectorClock) {
  if (values.length === 1) return values[0]
  const clockValues = values.map(value => vectorClock[value.metadata.id.toString('hex')] || 0)
  const maxValue = Math.max(...clockValues)
  return values[clockValues.indexOf(maxValue)]
}

function numberLinks (values) {
  const numLinks = values.map(value => value.links.length)
  const max = Math.max(...numLinks)
  const entriesWithMax = values.filter(value => value.links.length === max)
  if (entriesWithMax.length === 1) return entriesWithMax[0]
  return compareTimestamps(entriesWithMax)
}

function compareTimestamps (values) {
  const timestamps = values.map(value => value.timestamp)
  const max = Math.max(...timestamps)
  return values.find(value => value.timestamp === max)
}

function numberLinksOtherPeers (values, myKey) {
  // the number of links i know about from people other than me
  const valuesNotMyLinks = values.map((value) => {
    value.links.filter(link => link.split('@')[0] !== myKey)
  })
  return numberLinks(valuesNotMyLinks)
}

function numberLinksUniquePeers (values) {
  // the number of links i know about from distinct peers (including myself)
  const valuesUniqueLinks = values.map((value) => {
    var knownPeers = []
    value.links.filter((link) => {
      const peer = link.split('@')[0]
      if (knownPeers.indexOf(peer) < 0) {
        knownPeers.push(peer)
        return true
      }
      return false
    })
  })
  return numberLinks(valuesUniqueLinks)
}

module.exports = {
  dumbMerge,
  vectorClock,
  numberLinks,
  compareTimestamps,
  numberLinksOtherPeers,
  numberLinksUniquePeers
}
