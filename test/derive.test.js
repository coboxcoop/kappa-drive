const { describe } = require('tape-plus')
const crypto = require('hypercore-crypto')
const sodium = require('sodium-native')
const hypercore = require('hypercore')
const pump = require('pump')

const KappaDrive = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('replicate with derived keypair', (context) => {
  context('regular hypercore using derived keypair', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var master = masterKey()
    var keys = keyPair(master, 0)

    assert.ok(keys, 'generates keys')

    var core1 = hypercore(storage1, keys.publicKey, { secretKey: keys.secretKey })
    var core2 = hypercore(storage2, keys.publicKey)

    var dog = 'dog'
    core1.append(dog, (err, seq) => {
      assert.error(err, 'no error')
      var r1 = core1.replicate(true)
      var r2 = core2.replicate(false)
      pump(r1, r2, r1, (err) => {
        assert.error(err, 'no error')

        core2.get(0, (err, msg) => {
          assert.error(err, 'no error')
          assert.same(msg.toString(), dog, 'gets the dog')
          cleanup([storage1, storage2], next)
        })
      })
    })
  })

  context('derived parent key, defaults to latest value', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var master1 = masterKey()
    var master2 = masterKey()
    var keyPair1 = keyPair.bind(null, master1)
    var keyPair2 = keyPair.bind(null, master2)
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key, { deriveKeyPair: keyPair1 })
    var kdrive2 = KappaDrive(storage2, key, { deriveKeyPair: keyPair2 })

    write(kdrive, 'world', () => {
      sync(() => {
        write(kdrive2, 'mundo', () => sync(check))
      })
    })

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function check () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'mundo', 'gets latest value')
        write(kdrive2, 'verden', () => sync(checkAgain))
      })
    }

    function checkAgain () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'verden', 'gets latest value')
        cleanup([storage1, storage2], next)
      })
    }
  })
})

function masterKey () {
  const key = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
  sodium.crypto_kdf_keygen(key)
  return key
}

function keyPair (masterKey, id, ctxt = 'kappadrive') {
  const context = sodium.sodium_malloc(sodium.crypto_hash_sha256_BYTES)
  sodium.crypto_hash_sha256(context, Buffer.from(ctxt))
  const seed = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
  sodium.crypto_kdf_derive_from_key(seed, id, context.slice(0, sodium.crypto_kdf_CONTEXTBYTES), masterKey)
  return crypto.keyPair(seed)
}
