const { describe } = require('tape-plus')
const RAM = require('random-access-memory')
const crypto = require('hypercore-crypto')

const KappaDrive = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('multiwriter', (context) => {
  context('replicate to empty drive', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key)
    var kdrive2 = KappaDrive(storage2, key)

    write(kdrive, 'world', sync)

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync () {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error on replicate')
          kdrive.ready(() => kdrive2.ready(check))
        })
      }))
    }

    function check (err) {
      assert.error(err, 'no error')
      kdrive2.readFile('/hello.txt', 'utf-8', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data, 'world', 'gets latest value')
        cleanup([storage1, storage2], next)
      })
    }
  })

  context('defaults to latest value', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key)
    var kdrive2 = KappaDrive(storage2, key)

    write(kdrive, 'world', () => {
      sync(() => {
        write(kdrive2, 'mundo', () => sync(check))
      })
    })

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function check () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'mundo', 'gets latest value')
        write(kdrive2, 'verden', () => sync(checkAgain))
      })
    }

    function checkAgain () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'verden', 'gets latest value')
        cleanup([storage1, storage2], next)
      })
    }
  })

  context('defalts to latest value with file descriptors', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key)
    var kdrive2 = KappaDrive(storage2, key)

    write(kdrive, 'world', () => {
      write(kdrive2, 'mundo', () => sync(check))
    })

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.open('/hello.txt', 'w', (err, fd) => {
          assert.error(err, 'no error')
          drive.write(fd, Buffer.from(data), 0, 5, null, (err, bytesWritten) => {
            assert.error(err, 'no error')
            assert.same(bytesWritten, 5, 'correct number of bytes written')
            drive.close(fd, (err) => {
              assert.error(err, 'no error')
              cb()
            })
          })
        })
      })
    }

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function check () {
      kdrive.open('/hello.txt', 'r', (err, fd) => {
        assert.error(err, 'no error')
        assert.ok(fd, 'valid file descriptor')
        var data = Buffer.alloc('mundo'.length)
        kdrive.read(fd, data, 0, 5, null, (err, bytesRead, readData) => {
          assert.error(err, 'no error')
          assert.same(readData, Buffer.from('mundo'), 'gets latest value')
          cleanup([storage1, storage2], next)
        })
      })
    }
  })

  context('defaults to latest value with streams', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key)
    var kdrive2 = KappaDrive(storage2, key)

    write(kdrive, 'mundo', () => {
      write(kdrive2, 'verden', sync)
    })

    function write (d, data, cb) {
      d.ready((err) => {
        assert.error(err, 'no error')
        var ws = d.createWriteStream('/hello.txt')
        ws.on('finish', cb)
        ws.end(data)
      })
    }

    function sync () {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error')
          kdrive.ready(() => kdrive2.ready(check))
        })
      }))
    }

    function check () {
      var rs = kdrive.createReadStream('/hello.txt')
      rs.on('data', (data) => {
        assert.same(data, Buffer.from('verden'), 'gets latest value')
        cleanup([storage1, storage2], next)
      })
    }
  })
})
