const { describe } = require('tape-plus')
const RAM = require('random-access-memory')
const crypto = require('hypercore-crypto')
const fs = require('fs')

const KappaDrive = require('../')

const { tmp, cleanup } = require('./util')

describe('basic', (context) => {
  context('uses multifeed key for replication', (assert, next) => {
    var key = crypto.randomBytes(32)
    var drive = KappaDrive(RAM, key)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      assert.same(drive.key, key, 'drive key is key')
      assert.same(drive._feeds.key, key, 'multifeed key is key')
      assert.notEqual(drive.metadata.key, key, 'metadata key is different to key')
      assert.notEqual(drive.content.key, key, 'content key is different to key')
      next()
    })
  })

  context('write and read latest value', (assert, next) => {
    var drive = KappaDrive(RAM)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.readFile('/hello.txt', (err, content) => {
          assert.error(err, 'no error')
          assert.same(content, Buffer.from('world'))
          next()
        })
      })
    })
  })

  context('writeStream and readStream', (assert, next) => {
    var drive = KappaDrive(RAM)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      var ws = drive.createWriteStream('/hello.txt')
      ws.end('world')
      ws.on('finish', () => {
        var rs = drive.createReadStream('/hello.txt')
        rs.on('data', (data) => {
          assert.same(data, Buffer.from('world'))
          next()
        })
      })
    })
  })

  context('open', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/hello.txt', 'w+', function (err, fd) {
        assert.error(err, 'no error')
        assert.same(typeof fd, 'number', "returns a reference to the drive's file descriptor")
        cleanup(storage, next)
      })
    })
  })

  context('close', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/hello.txt', 'w+', function (err, fd) {
        assert.error(err, 'no error')
        drive.close(fd, function (err) {
          assert.error(err, 'closes without error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('write', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/bonjour.txt', 'w', function (err, fd) {
        assert.error(err, 'no error')
        var data = Buffer.from('monde')
        drive.write(fd, data, 0, 5, null, (err, bytesWritten, writeData) => {
          assert.error(err, 'drive.write returns no error')
          assert.same(bytesWritten, 'monde'.length, 'correct number of bytes written')
          // i think hyperdrive does not return the buffer in the cb, unlike fs.write
          // assert.same(writeData, Buffer.from('monde'))
          drive.close(fd, function (err) {
            assert.error(err, 'no error')
            drive.readFile('./bonjour.txt', (err, contents) => {
              assert.error(err, 'no error')
              assert.same(contents, Buffer.from('monde'), 'file successfully written')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('read', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/bonjour.txt', 'monde', (err) => {
        assert.error(err, 'no error')
        drive.open('/bonjour.txt', 'r', function (err, fd) {
          assert.error(err, 'no error')
          var data = Buffer.alloc('monde'.length)
          drive.read(fd, data, 0, 5, null, (err, bytesRead, readData) => {
            assert.error(err, 'drive.read returns no error')
            assert.same(readData, Buffer.from('monde'))
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('exists', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.exists('/hello.txt', function (bool) {
        assert.notOk(bool, 'no file yet')
        drive.writeFile('/hello.txt', 'world', function (err) {
          assert.error(err, 'no error')
          drive.exists('/hello.txt', function (bool) {
            assert.ok(bool, 'found file')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('stat', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.stat('/hello.txt', (err, stats) => {
          assert.error(err, 'no error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('lstat', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.lstat('/hello.txt', (err, stats) => {
          assert.error(err, 'no error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('symlink', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.symlink('/hello.txt', '/world.txt', (err) => {
          assert.error(err, 'no error')
          drive.readFile('/world.txt', (err, data) => {
            assert.error(err, 'no error')
            assert.same(data, Buffer.from('world'), 'symlinked')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('truncate', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.truncate('/hello.txt', 1, (err) => {
          assert.error(err, 'no error')
          drive.readFile('/hello.txt', (err, data) => {
            assert.error(err, 'no error')
            assert.deepEqual(Buffer.from('w'), data, 'truncated file')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('unlink', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.unlink('/hello.txt', (err) => {
          assert.error(err, 'no error')
          drive.readFile('/hello.txt', (err, data) => {
            assert.ok(err, 'file no longer exists')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  // TODO: symlink

  context('_update', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        // chmod a+x
        drive._update('/hello.txt', { mode: fs.constants.S_IXOTH }, (err) => {
          assert.error(err, 'no error')
          drive.stat('/hello.txt', (err, stat) => {
            assert.error(err, 'no error')
            assert.same(stat.mode, fs.constants.S_IXOTH)
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('mkdir and rmdir', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.mkdir('/hello', (err) => {
        assert.error(err, 'no error')
        drive.readdir('/', (err, files) => {
          assert.error(err, 'no error')
          assert.same(files, ['hello'], 'created directory is listed')
          drive.rmdir('/hello', (err) => {
            assert.error(err, 'no error')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('rename', (assert, next) => {
    var drive = KappaDrive(RAM)

    drive.ready(() => {
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.rename('/hello.txt', '/goodbye.txt', (err) => {
          assert.error(err, 'no error')
          drive.stat('/hello.txt', (err) => {
            assert.ok(err, 'statting source fail no longer possible')
            drive.readFile('/goodbye.txt', 'utf-8', (err, data) => {
              assert.error(err)
              assert.same(data, 'world', 'destination has correct content')
              next()
            })
          })
        })
      })
    })
  })

  context('keys', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'no error')
      assert.ok(Buffer.isBuffer(drive.key), 'drive.key returns a buffer')
      assert.ok(Buffer.isBuffer(drive.discoveryKey), 'drive.discoveryKey returns a buffer')
      cleanup(storage, next)
    })
  })

  context('readdir', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    const filesToWrite = ['stuff/things/ape.txt', 'badger_number_one.txt']
    const expectedReaddirOutput = ['stuff', 'badger_number_one.txt']
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile(filesToWrite[0], 'tree', (err) => {
        assert.error(err, 'no error')
        drive.writeFile(filesToWrite[1], 'peanut', (err) => {
          assert.error(err, 'no error')
          drive.readdir('/', (err, files) => {
            assert.error(err, 'no error')
            assert.deepEqual(files.sort(), expectedReaddirOutput.sort(), 'files are the same')
            drive.readdir('/stuff', (err, files) => {
              assert.error(err, 'no error')
              assert.equal('things', files[0], 'can specify directory')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('destroyStorage', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.destroyStorage((err) => {
        assert.error(err, 'No error on drive.destroyStorage')
        cleanup(storage, next)
      })
    })
  })
})
