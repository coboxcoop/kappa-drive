const { describe } = require('tape-plus')
const crypto = require('crypto')
const messages = require('../messages')

describe('schema.proto', (context) => {
  context('Feed', (assert, next) => {
    assert.ok(messages.State.Feed, 'has a schema for Feed')
    assert.throws(() => messages.State.Feed.encode({ dogSays: 'woof' }), 'Error: id is required', 'throws an error when missing id')
    assert.throws(() => messages.State.Feed.encode({ id: crypto.randomBytes(32).toString('hex'), dogSays: 'woof' }), 'Error: seq is required', 'throws an error when missing seq')

    var payload = messages.State.Feed.encode({ id: crypto.randomBytes(32).toString('hex'), seq: 0 })
    assert.ok(payload instanceof Buffer, 'is a buffer')

    next()
  })

  context('State', (assert, next) => {
    assert.ok(messages.State, 'has a schema for State')

    var message = { catSays: 'miaow' }
    assert.throws(() => messages.State.encode(message), 'Error: filename is required', 'throws an error when missing filename')

    message = { filename: 'hello.txt' }
    assert.throws(() => messages.State.encode(message), 'Error: links is required', 'throws an error when missing links')

    message = { filename: 'hello.txt', links: [] }
    assert.throws(() => messages.State.encode(message), 'Error: metadata is required', 'throws an error when missing metadata')

    message = { filename: 'hello.txt', links: [], metadata: { id: crypto.randomBytes(32).toString('hex'), seq: 0 } }
    assert.throws(() => messages.State.encode(message), 'Error: content is required', 'throws an error when missing content')

    message = {
      filename: 'hello.txt',
      links: [],
      metadata: { id: crypto.randomBytes(32).toString('hex'), seq: 0 },
      content: { id: crypto.randomBytes(32).toString('hex'), seq: 0 }
    }

    var payload = messages.State.encode(message)
    assert.ok(payload instanceof Buffer, 'is a buffer')

    next()
  })
})
