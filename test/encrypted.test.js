const { describe } = require('tape-plus')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const encoder = require('crypto-encoder')
const crypto = require('hypercore-crypto')
const fs = require('fs')
const path = require('path')
const pdf = require('html-pdf')

const KappaDrive = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('encrypted multiwriter', (context) => {
  context('autogen corestore: defaults to latest value', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var encryptionKey = crypto.randomBytes(32)
    var valueEncoding = encoder(encryptionKey, {
      nonce: encryptionKey.slice(0, encoder.NONCEBYTES),
      valueEncoding: 'binary'
    })
    var kdrive = KappaDrive(storage1, key, { valueEncoding })
    var kdrive2 = KappaDrive(storage2, key, { valueEncoding })

    write(kdrive, 'world', () => {
      sync(() => {
        write(kdrive2, 'mundo', () => sync(check))
      })
    })

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          drive.readFile('/hello.txt', (err, data) => {
            assert.error(err, 'no error')
            cb()
          })
        })
      })
    }

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error on replicate')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function check () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'mundo', 'gets latest value')
        write(kdrive2, 'verden', () => sync(checkAgain))
      })
    }

    function checkAgain () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'verden', 'gets latest value')
        return next()
        // return cleanup([storage1, storage2], next)
      })
    }
  })

  context('custom corestore', (assert, next) => {
    var storage1 = new Corestore(RAM)
    var storage2 = new Corestore(RAM)

    var key = crypto.randomBytes(32)
    var encryptionKey = crypto.randomBytes(32)
    var valueEncoding = encoder(encryptionKey, {
      nonce: encryptionKey.slice(0, encoder.NONCEBYTES),
      valueEncoding: 'binary'
    })
    var kdrive = KappaDrive(storage1, key, { valueEncoding })
    var kdrive2 = KappaDrive(storage2, key, { valueEncoding })

    write(kdrive, 'world', () => {
      sync(() => {
        write(kdrive2, 'mundo', () => sync(check))
      })
    })

    function write (drive, data, cb) {
      drive.ready((err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error on replicate')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function check () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'mundo', 'gets latest value')
        write(kdrive2, 'verden', () => sync(checkAgain))
      })
    }

    function checkAgain () {
      kdrive.readFile('/hello.txt', (err, data) => {
        assert.error(err, 'no error')
        assert.same(data.toString(), 'verden', 'gets latest value')
        return next()
      })
    }
  })

  // context('large files, e.g. a pdf', (assert, next) => {
  //   var storage1 = tmp()
  //   var storage2 = tmp()
  //   var key = crypto.randomBytes(32)
  //   var encryptionKey = crypto.randomBytes(32)
  //   var valueEncoding = encoder(encryptionKey, {
  //     nonce: encryptionKey.slice(0, encoder.NONCEBYTES),
  //     valueEncoding: 'binary'
  //   })
  //   var kdrive = KappaDrive(storage1, key, { valueEncoding })
  //   var kdrive2 = KappaDrive(storage2, key, { valueEncoding })

  //   var html = fs.readFileSync(path.join(__dirname, './fixtures/template.html'), 'utf8')
  //   pdf.create(html).toBuffer((err, data) => {
  //     let copy = Buffer.from(data)
  //     write(kdrive, data, () => sync(check))

  //     function check () {
  //       kdrive2.readFile('/data.pdf', (err, buf) => {
  //         assert.error(err, 'no error')
  //         assert.same(copy.toString('utf8'), buf.toString('utf8'), 'gets latest value')
  //         return cleanup([storage1, storage2], next)
  //       })
  //     }
  //   })

  //   function write (drive, data, cb) {
  //     drive.ready((err) => {
  //       assert.error(err, 'no error')
  //       drive.writeFile('/data.pdf', data, (err) => {
  //         assert.error(err, 'no error')
  //         cb()
  //       })
  //     })
  //   }

  //   function sync (cb) {
  //     kdrive.ready(() => kdrive2.ready(() => {
  //       replicate(kdrive, kdrive2, (err) => {
  //         assert.error(err, 'no error on replicate')
  //         kdrive.ready(() => kdrive2.ready(cb))
  //       })
  //     }))
  //   }
  // })
})
