// const { describe } = require('tape-plus')
// const crypto = require('hypercore-crypto')
// const KappaDrive = require('../')
// const Corestore = require('corestore')
// const RAM = require('random-access-memory')
// const { tmp, cleanup } = require('./util')
// const CoreNetworker = require('corestore-networker')
// const FeedNetworker = require('@frando/corestore-multifeed/networker')
// const memdb = require('level-mem')
// const level = require('level')
// const mkdirp = require('mkdirp')

// describe('shared corestore networker', (context) => {
//   context('one drive to one drive', (assert, next) => {
//     const key = crypto.randomBytes(32)
//     const storage1 = tmp()
//     const storage2 = tmp()

//     // peer 1 sets up
//     const corestore1 = new Corestore(storage1)
//     const network1 = new CoreNetworker(corestore1)
//     const drive1 = KappaDrive(corestore1, key)

//     // peer 2 sets up
//     const corestore2 = new Corestore(storage2)
//     const network2 = new CoreNetworker(corestore2)
//     const drive2 = KappaDrive(corestore2, key)

//     // start listening for changes
//     swarm(network1, drive1)
//     swarm(network2, drive2)

//     // peer 1 writes
//     write(drive1, 'world', () => {
//       wait(1000, () => {
//         check(() => {
//           write(drive2, 'mundo', () => {
//             wait(1000, checkAgain)
//           })
//         })
//       })
//     })

//     function read (drive, cb) {
//       drive.ready(() => drive.readFile('/hello.txt', cb))
//     }

//     function write (drive, data, cb) {
//       drive.ready((err) => {
//         assert.error(err, 'no error')
//         drive.writeFile('/hello.txt', data, (err) => {
//           assert.error(err, 'no error')
//           network1.close()
//           network2.close()
//           cb()
//         })
//       })
//     }

//     function wait (ms, cb) {
//       setTimeout(cb, ms)
//     }

//     function check (cb) {
//       read(drive2, (err, data) => {
//         assert.error(err, 'no error')
//         assert.same(data && data.toString(), 'world', 'replicates successfully')
//         cb()
//       })
//     }

//     function checkAgain () {
//       read(drive1, (err, data) => {
//         assert.error(err, 'no error')
//         assert.same(data && data.toString(), 'mundo', 'replicates successfully')
//         next()
//       })
//     }

//     function swarm (network, drive, opts = {}) {
//       network.listen()
//       const multinet = new FeedNetworker(network)
//       return multinet.swarm(drive._feeds, { live: true, ...opts })
//     }
//   })

//   context('two drives to two drives', (assert, next) => {
//     const storage1 = tmp()
//     const storage2 = tmp()
//     const key1 = crypto.randomBytes(32)
//     const key2 = crypto.randomBytes(32)

//     const corestore1 = new Corestore(storage1)
//     const network1 = new CoreNetworker(corestore1)

//     const corestore2 = new Corestore(storage2)
//     const network2 = new CoreNetworker(corestore2)

//     const drive1a = KappaDrive(corestore1, key1)
//     const drive1b = KappaDrive(corestore2, key1)
//     const drive2a = KappaDrive(corestore1, key2)
//     const drive2b = KappaDrive(corestore2, key2)

//     swarm(network1, drive1a)
//     swarm(network2, drive1b)
//     swarm(network1, drive2a)
//     swarm(network2, drive2b)

//     write(drive1a, '/hello.txt', 'world', () => {
//       write(drive2a, '/world.txt', 'hello', () => {
//         wait(1000, check)
//       })
//     })

//     function write (drive, filename, data, cb) {
//       drive.ready((err) => {
//         assert.error(err, 'no error')
//         drive.writeFile(filename, data, (err) => {
//           assert.error(err, 'no error')
//           cb()
//         })
//       })
//     }

//     function read (drive, filename, cb) {
//       drive.ready(() => drive.readFile(filename, cb))
//     }

//     function wait (ms, cb) {
//       setTimeout(cb, ms)
//     }

//     function check () {
//       read(drive1b, '/hello.txt', (err, data) => {
//         assert.error(err, 'no error')
//         assert.same(data && data.toString(), 'world', 'drive1b replicates successfully')

//         read(drive1b, '/world.txt', (err, data) => {
//           assert.error(err, 'no error')
//           assert.same(data && data.toString(), 'hello', 'drive2b replicates successfully')
//           network1.close()
//           network2.close()
//           cleanup([storage1, storage2], next)
//         })
//       })
//     }

//     function swarm (network, drive, opts = {}) {
//       network.listen()
//       const multinet = new FeedNetworker(network)
//       return multinet.swarm(drive._feeds, { live: true, ...opts })
//     }
//   })
// })

// function defaultViews (storage) {
//   if (isFunction(storage)) return memdb()
//   var views = path.join(path.resolve(storage), 'views')
//   mkdirp.sync(views)
//   return level(views)
// }

