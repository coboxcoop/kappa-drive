const { describe } = require('tape-plus')
const crypto = require('hypercore-crypto')
const KappaDrive = require('../')
const { replicate, tmp, cleanup } = require('./util')

describe('conflict', (context) => {
  context('fork', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)
    var kdrive = KappaDrive(storage1, key)
    var kdrive2 = KappaDrive(storage2, key)

    kdrive.ready((err) => {
      assert.error(err, 'no error')
      kdrive2.ready(() => {
        var ws = kdrive.createWriteStream('/hello.txt')
        ws.end('world')
        ws.on('finish', () => sync(writeFork))
      })
    })

    function sync (cb) {
      kdrive.ready(() => kdrive2.ready(() => {
        replicate(kdrive, kdrive2, (err) => {
          assert.error(err, 'no error on replicate')
          kdrive.ready(() => kdrive2.ready(cb))
        })
      }))
    }

    function writeFork () {
      var pending = 2
      var done = () => {
        !--pending && sync(checkFork)
      }
      var ws = kdrive.createWriteStream('/hello.txt')
      ws.on('finish', done)
      ws.end('mundo')
      var ws2 = kdrive2.createWriteStream('/hello.txt')
      ws2.on('finish', done)
      ws2.end('verden')
    }

    function checkFork () {
      var ws = kdrive.createWriteStream('/hello.txt')
      ws.end('whateverr')
      ws.on('finish', () => {
        kdrive.ready(() => {
          kdrive2.readFile('/hello.txt', 'utf-8', (err, data) => {
            assert.error(err, 'no error')
            assert.same(data, 'verden')
            kdrive.readFile('/hello.txt', 'utf-8', (err, data) => {
              assert.error(err, 'no error')
              assert.same(data, 'whateverr', 'forked values')
              cleanup([storage1, storage2], next)
            })
          })
        })
      })
    }
  })
})
