const rimraf = require('rimraf')
const debug = require('debug')('cleanup')
const tmpdir = require('tmp').dirSync
const mkdirp = require('mkdirp')
const { spawn } = require('child_process')

function cleanup (dirs, cb) {
  if (!cb) cb = noop
  if (!Array.isArray(dirs)) dirs = [dirs]
  var pending = 1

  function next (n) {
    var dir = dirs[n]
    if (!dir) return done()
    ++pending
    process.nextTick(next, n + 1)

    rimraf(dir, (err) => {
      if (err) return done(err)
      done()
    })
  }

  function done (err) {
    if (err) {
      pending = Infinity
      return cb(err)
    }
    if (!--pending) return cb()
  }

  next(0)
}

function tmp () {
  var path = '.' + tmpdir().name
  mkdirp.sync(path)
  return path
}

function replicate (drive1, drive2, cb) {
  if (!cb) cb = noop
  var s = drive1.replicate(true)
  var d = drive2.replicate(false)

  var pending = 2

  s.pipe(d).pipe(s)
    .on('remote-feeds', done)
    .on('remote-feeds', done)

  function done (key, err) {
    debug('replicate: ', err ? 'FAIL' : 'SUCCESS')
    if (err) return cb(err)
    setTimeout(() => {
      if (!--pending) {
        s.end()
        cb()
      }
    }, 50)
  }
}

function uniq (array) {
  if (!Array.isArray(array)) array = [array]
  return Array.from(new Set(array))
}

function noop () {}

function run (command, cb) {
  const child = spawn('sh', ['-c', command])
  var output = []
  child.stdout.on('data', (data) => { output.push(data.toString()) })

  child.stderr.on('data', (data) => { output.push(data.toString()) })

  child.on('exit', function (code) {
    if (code) return cb(new Error('child process exited with error ' + code))
    cb(null, output)
  })
}

module.exports = { cleanup, tmp, replicate, uniq, run }
